# Getting started with the P-NUCLEO-LRWAN3 starter pack

source : [here](https://www.st.com/content/ccc/resource/technical/document/user_manual/group1/01/0e/9a/df/16/73/42/51/DM00620948/files/DM00620948.pdf/jcr:content/translations/en.DM00620948.pdf)

## LRWAN_NS1 LoRa® LF band and sensor expansion board

*By default, USART1 (PA9/PA10) is used in the NUCLEO-L073RZ board to control the RHF0M003-LF20 modem.
Optionally, it is possible to use USART2 (PA2/PA3) via jumper resistor on the LRWAN_NS1. Refer to its user
manual. If USART2 (PA2/PA3) is used to control the modem, the following solder bridge on the Nucleo board
must be configured accordingly:
• SB62 and SB63 are closed
• SB13 and SB14 are opened to disconnect the STM32 UART from ST-LINK
Refer to [5] in the USART Communication section for more details.*

## LRWAN_NS1 sensor device setup and configuration

by default, the device is configured for the CN470Prequel frequency band and in the OTAA mode.

The LRWAN_NS1 LORA expansion board uses the serial interface (CN8 pin 1 (TX) and pin2 (RX)) for the AT command console. The default serial configuration is :
* Baud rate: 9600
* Data: 8 bit
* Parity: None
* Stop: 1 bit

### Terminal emulation software settings

* When receive a new line : CR (\r)
* When transmit a new line : CR+LF (\r\n)

### AT command list
*Syntax : AT+CMD*
* AT 
* FDEFAULT -> Resets to factory default settings
* RESET -> Software reset gateway
* DFU
* LOWPOWER
* VER
* MSG
* MSGHEX
* CMSG
* CMSGHEX
* PMSG
* PMSGHEX
* CH
* DR
* ADR
* REPT
* RETRY
* POWER
* RXWIN2
* RXWIN1
* PORT
* MODE
* ID
* KEY
* CLASS
* JOIN
* LW
* TEST
* UART
* DELAY
* VDD
* RTC
* EEPROM