# Tests done

## A simple "Hello-World" RIOT application 

    #include <stdio.h>
    #include "xtimer.h"

    int main(void)
    {
        xtimer_sleep(5);
        puts("Hello from RIOT!");
        return 0;
    }


## Send an AT command

* Tried to compile the RIOT/tests/driver_at/main.c on our board
* Made our own mini test program 

        #include <stdio.h>
        #include <string.h>
        #include <stdlib.h>
        #include <periph/uart.h>

        #include "at.h"
        #include "shell.h"
        #include "xtimer.h"

        int main(void)
        {
            xtimer_sleep(5);
            puts("Hello from RIOT!");
            
        at_dev_t *dev = NULL;
        char *buf;
        buf = malloc(256*sizeof(char));

        int init = at_dev_init( dev, UART_DEV(1) , 9600, buf, 256);

        if(init == UART_OK) {
            puts("OK");
        }
        else{
            puts("NO OK");
        }

        at_send_cmd_wait_ok	(dev, "AT", 10 * US_PER_SEC);

        return 0;
        }

* Did not get the result expected for the moment

## 16/03

modify the function *cmd_send* in file [periph_uart](https://github.com/RIOT-OS/RIOT/blob/master/tests/periph_uart/main.c)

    static int cmd_send(int argc, char **argv)
    {
    int dev;
    uint8_t endline = (uint8_t)'\n';
    uint8_t carrier_return = (uint8_t)'\r';


    if (argc < 3) {
        printf("usage: %s <dev> <data (string)>\n", argv[0]);
        return 1;
    }
    /* parse parameters */
    dev = parse_dev(argv[1]);
    if (dev < 0) {
        return 1;
    }

    printf("UART_DEV(%i) TX: %s\n", dev, argv[2]);
    uart_write(UART_DEV(dev), (uint8_t *)argv[2], strlen(argv[2]));
    uart_write(UART_DEV(dev), &carrier_return, 1);
    uart_write(UART_DEV(dev), &endline, 1);
    return 0;
    }

## 22/03

Compiled and flashed the file [periph_uart](https://github.com/RIOT-OS/RIOT/blob/master/tests/periph_uart/main.c)

    > init 2 115200 
    2021-03-22 17:33:51,021 # init 2 115200
    2021-03-22 17:33:51,026 # Success: Initialized UART_DEV(2) at BAUD 115200</br>
    2021-03-22 17:33:51,282 # UARD_DEV(2): test uart_poweron() and uart_poweroff()  ->  [OK]

    > send 2 test
    2021-03-22 17:34:00,162 # send 2 test
    2021-03-22 17:34:00,165 # UART_DEV(2) TX: test

    > 2021-03-22 17:34:11,919 # Success: UART_DEV(2) RX: [Hello from Minicom]\n

    Receive on minicom :
    test

Modify the file https://github.com/RIOT-OS/RIOT/blob/master/drivers/include/at.h

    /**
    * @brief End of line character to send after the AT command.
     */
    #ifndef CONFIG_AT_SEND_EOL
    #define CONFIG_AT_SEND_EOL "\r\n"
    #endif

this variable is used in the function *at_send_cmd* in this file : https://github.com/RIOT-OS/RIOT/blob/master/drivers/at/at.c

    int at_send_cmd(at_dev_t *dev, const char *command, uint32_t timeout)
    {
        size_t cmdlen = strlen(command);

        uart_write(dev->uart, (const uint8_t *)command, cmdlen);
        uart_write(dev->uart, (const uint8_t *)CONFIG_AT_SEND_EOL, AT_SEND_EOL_LEN);

        if (AT_SEND_ECHO) {
            if (at_expect_bytes(dev, command, timeout)) {
                return -1;
            }

            if (at_expect_bytes(dev, CONFIG_AT_SEND_EOL AT_RECV_EOL_1 AT_RECV_EOL_2, timeout)) {
                return -2;
            }
        }

        return 0;
    }

Compiled and flashed the file https://github.com/RIOT-OS/RIOT/blob/master/tests/driver_at/main.c

    init 1 9600
    2021-03-27 15:46:14,093 # init 1 9600
    > send AT
    2021-03-27 15:46:23,677 # send AT
    2021-03-27 15:46:33,705 # Error

try to comment the following lines in [at.h](https://github.com/RIOT-OS/RIOT/blob/master/drivers/include/at.h)

    /**
    * @brief 1st end of line character received (S4 aka LF character for a modem).
    */
    // #ifndef AT_RECV_EOL_2
    // #define AT_RECV_EOL_2   "\n"
    // #endif

because the end of line character received need to be CR

## 29/03
### Compiled and flashed the code provided by stm32 ([LoRaWAN software expansion for STM32Cube](https://www.st.com/en/embedded-software/i-cube-lrwan.html)).

Plugged in the USBSerial adapter (inversed TX and RX) on PA9 and PA10.

Results : 

![alt text](IMG/code_STM32_results.png "Title")

Notice the 4 special characte "????" before each AT commands transmit. they are corresponding to 0xFFFFFFFF.

### Send an AT command directly on the LoRaWAN module with coolterm.

![alt text](IMG/montage.jpg "Title")
![alt text](IMG/coolterm_options.png "Title")
![alt text](IMG/coolterm_results.png "Title")

### Modified file [at.c](https://github.com/RIOT-OS/RIOT/blob/master/drivers/at/at.c)

added 0xFFFFFFFF before each command send

    int at_send_cmd(at_dev_t *dev, const char *command, uint32_t timeout)
    {
    [...]
    uint32_t wakeup = 0xFFFFFFFF;

    uart_write(dev->uart, (const uint8_t *) &wakeup, 4);
    uart_write(dev->uart, (const uint8_t *)command, cmdlen);
    uart_write(dev->uart, (const uint8_t *)CONFIG_AT_SEND_EOL, AT_SEND_EOL_LEN);
    
    [...]
    }   

Again the same result : 

    init 1 9600
    2021-03-27 15:46:14,093 # init 1 9600
    > send AT
    2021-03-27 15:46:23,677 # send AT
    2021-03-27 15:46:33,705 # Error

### Modified file [rn2xx3](https://github.com/RIOT-OS/RIOT/tree/master/drivers/rn2xx3)

    int autooff(rn2xx3_t *dev)
    {
        size_t p = snprintf(dev->cmd_buf, sizeof(dev->cmd_buf) - 1,
                            "%c%c%c%cAT",
                            0xFF, 0xFF, 0xFF, 0xFF)
                            ;
                            
        dev->cmd_buf[p] = 0;

        int ret = rn2xx3_write_cmd(dev);
        if (ret != RN2XX3_OK) {
            DEBUG("[rn2xx3] AT failed\n");
            return ret;
        }

        return ret;
    }

    /*
    * Driver's "public" functions
    */
    void rn2xx3_setup(rn2xx3_t *dev, const rn2xx3_params_t *params)
    {
        assert(dev && (params->uart < UART_NUMOF));

        /* initialize device parameters */
        dev->p = *params;

        /* initialize pins and perform hardware reset */
        if (gpio_is_valid(dev->p.pin_reset)) {
            gpio_init(dev->p.pin_reset, GPIO_OUT);
            gpio_set(dev->p.pin_reset);
        }

        /* UART is initialized later, since interrupts cannot be handled yet */

        autooff(dev);
    }

Results :

    2021-04-05 00:33:26,127 # main(): This is RIOT! (Version: 2021.04-devel-1044-gfd36c-wip/lrwan3/endpoint)
    2021-04-05 00:33:26,130 # RN2XX3 device driver test
    2021-04-05 00:33:26,132 # [rn2xx3] CMD: ����AT
    2021-04-05 00:33:26,134 # [rn2xx3] new state: CMD
    2021-04-05 00:33:31,140 # [rn2xx3] response timeout
    2021-04-05 00:33:31,142 # [rn2xx3] AT failed
    2021-04-05 00:33:31,144 # [rn2xx3] new state: RESET
    2021-04-05 00:33:31,148 # [rn2xx3] init: initialization successful
    2021-04-05 00:33:31,151 # Initialization OK, starting shell now

AT+JOIN :

    int rn2xx3_mac_join_network(rn2xx3_t *dev, loramac_join_mode_t mode)
    {
        size_t p = snprintf(dev->cmd_buf, sizeof(dev->cmd_buf) - 1,
                            "%c%c%c%cAT+JOIN=%s",
                            0xFF, 0xFF, 0xFF, 0xFF,
                            (mode == LORAMAC_JOIN_OTAA) ? "1" : "0");
                            
        dev->cmd_buf[p] = 0;

        int ret = rn2xx3_write_cmd(dev);
        if (ret != RN2XX3_OK) {
            DEBUG("[rn2xx3] join procedure command failed\n");
            return ret;
        }

        rn2xx3_set_internal_state(dev, RN2XX3_INT_STATE_MAC_JOIN);

        ret = rn2xx3_wait_reply(dev,
                                CONFIG_LORAMAC_DEFAULT_JOIN_DELAY1 + \
                                CONFIG_LORAMAC_DEFAULT_JOIN_DELAY2);

        rn2xx3_set_internal_state(dev, RN2XX3_INT_STATE_IDLE);

        return ret;
    }

Result :

    > mac join otaa
    2021-04-05 00:51:48,564 # mac join otaa
    2021-04-05 00:51:48,567 # [rn2xx3] CMD: ����AT+JOIN=1
    2021-04-05 00:51:48,569 # [rn2xx3] new state: CMD
    2021-04-05 00:51:48,616 # ffffffff2b4a4f494e3a204552524f52282d3129 d a[rn2xx3] RESP: ����+JOIN: ERROR(-1)
    2021-04-05 00:51:48,619 # [rn2xx3] new state: IDLE
    2021-04-05 00:51:48,623 # [rn2xx3] RET: 1, RESP: ����+JOIN: ERROR(-1)
    2021-04-05 00:51:48,626 # [rn2xx3] join procedure command failed

AT :

    int rn2xx3_mac_join_network(rn2xx3_t *dev, loramac_join_mode_t mode)
    {
        size_t p = snprintf(dev->cmd_buf, sizeof(dev->cmd_buf) - 1,
                            "%c%c%c%cAT",
                            0xFF, 0xFF, 0xFF, 0xFF
                            );
                            
        dev->cmd_buf[p] = 0;

        int ret = rn2xx3_write_cmd(dev);
        if (ret != RN2XX3_OK) {
            DEBUG("[rn2xx3] join procedure command failed\n");
            return ret;
        }

        rn2xx3_set_internal_state(dev, RN2XX3_INT_STATE_MAC_JOIN);

        ret = rn2xx3_wait_reply(dev,
                                CONFIG_LORAMAC_DEFAULT_JOIN_DELAY1 + \
                                CONFIG_LORAMAC_DEFAULT_JOIN_DELAY2);

        rn2xx3_set_internal_state(dev, RN2XX3_INT_STATE_IDLE);

        return ret;
    }


Result :
 
    > mac join otaa
    2021-04-05 00:50:06,598 # mac join otaa
    2021-04-05 00:50:06,601 # coucou[rn2xx3] CMD: ����AT
    2021-04-05 00:50:06,603 # [rn2xx3] new state: CMD
    2021-04-05 00:50:06,631 # ffffffff2b41543a204f4b d a[rn2xx3] RESP: ����+AT: OK
    2021-04-05 00:50:06,634 # [rn2xx3] new state: IDLE
    2021-04-05 00:50:06,637 # [rn2xx3] RET: 1, RESP: ����+AT: OK


### Modified file [periph_uart](https://github.com/RIOT-OS/RIOT/blob/master/tests/periph_uart/main.c)

added 0xFFFFFFFF before each command send

    static int cmd_send(int argc, char **argv)
    {
        int dev;
        uint8_t endline = (uint8_t)'\n';
        uint8_t carrier_return = (uint8_t)'\r';
        
        [....]
        printf("UART_DEV(%i) TX: %s\n", dev, argv[2]);
        uint32_t wakeup = 0xFFFFFFFF;
        uart_write(UART_DEV(dev), (uint8_t *) &wakeup, 4); // FF FF FF FF
        uart_write(UART_DEV(dev), (uint8_t *)argv[2], strlen(argv[2])); //CMD
        uart_write(UART_DEV(dev), &carrier_return, 1); //CR
        uart_write(UART_DEV(dev), &endline, 1); // LF
        return 0;
    }

Results : 

![alt text](IMG/send_AT.png "Title")


### Test driver HTS221, LIS3MDL, LPS22HB (sensors on the LRWAN_NS1 expansion board)

![alt text](IMG/hts221.png "Title")
![alt text](IMG/lis3mdl.png "Title")
![alt text](IMG/lps22hb.png "Title")