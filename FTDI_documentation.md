# USB serial adapter

![alt text](IMG/FTDI.jpg "Title")

## Color matching

**Red :** VCC </br> *Be careful not to connect this red wire if the card is already powered.* </br>
**White :** TX </br>
**Green :** RX </br>
**Black :** GND </br>

## Connection example

![alt text](IMG/connection_example.jpg "Title")

To know on wich pins to connect, see the following [link](https://os.mbed.com/platforms/ST-Nucleo-L073RZ/)

UART defined by RIOT for board nucleo-l073rz (defined in file [periph_conf](https://github.com/RIOT-OS/RIOT/blob/master/boards/nucleo-l073rz/include/periph_conf.h)):



    /**
    * @name    UART configuration
    * @{
    */
    static const uart_conf_t uart_config[] = {
        {
            .dev        = USART2,
            .rcc_mask   = RCC_APB1ENR_USART2EN,
            .rx_pin     = GPIO_PIN(PORT_A, 3),
            .tx_pin     = GPIO_PIN(PORT_A, 2),
            .rx_af      = GPIO_AF4,
            .tx_af      = GPIO_AF4,
            .bus        = APB1,
            .irqn       = USART2_IRQn,
            .type       = STM32_USART,
            .clk_src    = 0, /* Use APB clock */
        },
        {
            .dev        = USART1,
            .rcc_mask   = RCC_APB2ENR_USART1EN,
            .rx_pin     = GPIO_PIN(PORT_A, 10),
            .tx_pin     = GPIO_PIN(PORT_A, 9),
            .rx_af      = GPIO_AF4,
            .tx_af      = GPIO_AF4,
            .bus        = APB2,
            .irqn       = USART1_IRQn,
            .type       = STM32_USART,
            .clk_src    = 0, /* Use APB clock */
        },
        
    #ifdef MODULE_PERIPH_LPUART
        {
            .dev        = LPUART1,
            .rcc_mask   = RCC_APB1ENR_LPUART1EN,
            .rx_pin     = GPIO_PIN(PORT_C, 11),
            .tx_pin     = GPIO_PIN(PORT_C, 10),
            .rx_af      = GPIO_AF0,
            .tx_af      = GPIO_AF0,
            .bus        = APB1,
            .irqn       = LPUART1_IRQn,
            .type       = STM32_LPUART,
            .clk_src    = 0, /* Use APB clock */
        },
    #endif
    };

Once plugged in, you have to connect your USB serial adapter to minicom. Then you can communicate with your card.

Be sure to finish your command with **CTRL+J** on minicom.